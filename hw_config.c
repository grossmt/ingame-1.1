
#include "hw_config.h"
#define VECT_TAB_OFFSET  0x0

/*******************************************************************************
* Function Name  : GPIO_Configuration
* Description    : Configures the different GPIO ports.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void GPIO_Configuration(void)
{
  //GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOA  , ENABLE);
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOB , ENABLE);
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOC , ENABLE);
  RCC_APB2PeriphClockCmd(	 RCC_APB2Periph_SYSCFG, ENABLE);

	PIN_CONFIGURATION(LED1);	
	PIN_CONFIGURATION(LED2);
	PIN_CONFIGURATION(LED3);
	PIN_CONFIGURATION(LED4);
	PIN_CONFIGURATION(LED5);
	PIN_CONFIGURATION(LED6);
	PIN_CONFIGURATION(LED7);
	PIN_CONFIGURATION(LED8);
	
	PIN_CONFIGURATION(KEY1P);
	PIN_CONFIGURATION(KEY2P);
	PIN_CONFIGURATION(KEY3P);
	PIN_CONFIGURATION(KEY4P);
	
	PIN_CONFIGURATION(SW1);
	PIN_CONFIGURATION(SW2);
	PIN_CONFIGURATION(SW3);
	PIN_CONFIGURATION(SW4);
	PIN_CONFIGURATION(SW5);
	PIN_CONFIGURATION(SW6);
	PIN_CONFIGURATION(SW7);
	PIN_CONFIGURATION(SW8);
	
	PIN_CONFIGURATION(BLIGHT);
}
/*******************************************************************************
* Function Name  : Set_HWSystem
* Description    : Configures Main system clocks & power.
* Input          : None.
* Return         : None.
*******************************************************************************/
void Set_HWSystem(void)
{
	SystemCoreClockUpdate();
  GPIO_Configuration();
  //USART_Configuration();
	Timer_Configuration();
	DACinit();
	wh_lcd_init();
}
/*-------------------------END-------------------------------*/


