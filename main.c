#include "main.h"
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
volatile static uint8_t	playMode = BR, changeMode = 0;
Player player[8];

volatile  uint32_t pause_time;
volatile  uint32_t current_time;
volatile  uint32_t prev_time;
volatile  char state=0;
volatile 	uint8_t volume = 1;
uint8_t 	queue[8];

int who_pressed = -1;

const  Player InitPlayer=
{
	1, //enabled
	0, //keypressed
	0, //key state
	0, //timestamp
	0  //LED state
};



/*******************************************************************************
* Function Name  : keyPauseHandler
* Description    : Event Handler for PAUSE button
* Input          : None
* Output         : None
* Return         : 1 - PAUSE was pressed
									 0 - PAUSE was NOT pressed
*******************************************************************************/

char keyPauseHandler(void)
{
	//checking for press PAUSE	
	if (PIN_SIGNAL(KEY2P)==0) //keyPAUSE was pressed
	{
		vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
		if (PIN_SIGNAL(KEY2P)==0) //debounce protect,PAUSE was pressed
		{
			#ifdef USE_WH1602	
			wh_write_str_center("   �����   ",LINE1);
			#endif

			state=PAUSED;
			pause_time = current_time;
			BEEP(300, 10,volume);
			return 1;
		}
	}
	return 0;
}

/*******************************************************************************
* Function Name  : keyResetHandler
* Description    : Event Handler for RESET button
									 Clear the second line of LCD
									 Reset players' state
* Input          : None
* Output         : None
* Return         : 1 - RESET was pressed
									 0 - RESET was NOT pressed
*******************************************************************************/
char keyResetHandler(void)
{
	char i = 0;

	//waiting for press RESET	
	if (PIN_SIGNAL(KEY4P)==0) //keyRESET was pressed
	{
		vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
		if (PIN_SIGNAL(KEY4P)==0) //debounce protect,RESET was pressed
		{

			for (i=0;i<8;i++) 	
				player[i]=InitPlayer;

			who_pressed = -1;

			#ifdef USE_WH1602	
				wh_write_str_center("                ",LINE1);
			#endif	

			state=RESETED;

			BEEP(500,10,volume);
			while (PIN_SIGNAL(KEY4P)==0)
			{
				who_pressed = -1;
			}

			return 1;
		}
	}	
	return 0;
}
/*******************************************************************************
* Function Name  : keyStartHandler
* Description    : Event Handler for START button
									 Clear the second line of LCD
									 Reset players' ledstate
									 Sound of START
									 Set time to countdown

* Input          : tSTATE - type of discuss (COUNTDOWN or RECOUNTDOWN
									 tTIME - countdown from this time  
* Output         : None
* Return         : 1 - START was pressed
									 0 - START was NOT pressed
*******************************************************************************/
char keyStartHandler(char tSTATE, u32 tTIME)
{
	char i = 0;
	//waiting for press START	
	if (PIN_SIGNAL(KEY3P)==0) //keyStart was pressed
	{
		vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
		if (PIN_SIGNAL(KEY3P)==0) //debounce protect,START was pressed
		{
			for (i=0;i<8;i++) 
				player[i].ledstate = NOTHING;															

			who_pressed = -1;
			state=tSTATE;
			BEEP(1000, 100,volume);
			prev_time = 0;
			SetGameTimer(tTIME);

			return 1;
		}
	}	
	return 0;
}

/*******************************************************************************
* Function Name  : timeOverHandler
* Description    : Event Handler for FINISH state
									 Sound of FINISH

* Input          : timeover - compare current time to this value
* Output         : None
* Return         : 1 - Time is over
									 0 - Time is NOT over
*******************************************************************************/
int timeOverHandler(u32 timeover)
{
	if (current_time >= timeover*100)
	{
		state = FINISH;
		BEEP(500, 100,volume);

		#ifdef USE_WH1602	
		wh_write_str_center("����� ��������",LINE1);
		#endif

		return 1;
	}
	return 0;			
}


/*******************************************************************************
* Function Name  : timeEndingHandler
* Description    : Sound of BEEP at signalTime

* Input          : signalTime - compare current time to this value
* Output         : None
* Return         : None 
*******************************************************************************/
void timeEndingHandler(u32 signalTime)
{
	signalTime *=100;

	if (current_time >= signalTime && current_time <= signalTime+10)
	{
		BEEP(750, 30,volume);
	}
}
/*******************************************************************************
* Function Name  : printTime
* Description    : print current Time at the second line of LCD

* Input          : None
* Output         : None
* Return         : None 
*******************************************************************************/
void printTime()
{
	char str[16];
	current_time = GetGameTimer();

	if (current_time - prev_time >=100 )
	{
		memset(str,0, sizeof(str));
		sprintf(str,"   �����: %d   ",current_time/100);
		
		#ifdef USE_WH1602	
		wh_write_str_center(str,LINE1);
		#endif
		
		prev_time = current_time;
	}	
}

/*******************************************************************************
* Function Name  : printPlayerTimestamp
* Description    : print player's Time of answer at the second line of LCD

* Input          : nplayer - number of player
* Output         : None
* Return         : None 
*******************************************************************************/
void printPlayerTimestamp(uint8_t nplayer)
{
	char str[16];
	memset(str,0, sizeof(str));
	
	if (state == FALSTART)
	{
			sprintf(str, " P%d falstarted ", nplayer);
	}
	else 
	if (state == ANSWER)
	{
		sprintf(str, " P#%d TIME:%d.%02d ",nplayer,player[nplayer-1].keytimestamp/100,player[nplayer-1].keytimestamp%100);
	}
	
	#ifdef USE_WH1602	
		wh_write_str_center(str,LINE1);
	#endif
}


/*******************************************************************************
* Function Name  : keyResetVSHandler
* Description    : Manage queue of players if RESET is pressed

* Input          : top - index of last element of queue
									 bottom - index of first element of queue
* Output         : None
* Return         : -2 - queue is empty
									 bottom - index of first element of queue	
*******************************************************************************/
int keyResetVSHandler(int top,int bottom)
{
	int i = 0;

	if (PIN_SIGNAL(KEY4P)==0) //keyRESET was pressed
	{
		vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
		if (PIN_SIGNAL(KEY4P)==0) //debounce protect,RESET was pressed
		{
			if (top == bottom + 1 || top == bottom)
			{
				//no players in queue to answer
				//clear all
				for (i=0; i<8; i++)
				queue[i] = 0;
				
				for (i=0;i<8;i++) 	
					player[i]=InitPlayer;
				who_pressed = -1;

				#ifdef USE_WH1602	
				wh_write_str_center("                ",LINE1);
				#endif	

				state=RESETED;
				BEEP(500,10,volume);
				while (PIN_SIGNAL(KEY4P)==0)
				{
					who_pressed = -1;
				}								
				return -2;
			}
			else
			{
				//remove top of queue
				//queue[bottom+1] = 0;

				player[queue[bottom+1]-1].ledstate = NOTHING;
				bottom ++;	

				while (PIN_SIGNAL(KEY4P)==0)
				{
					queue[bottom] = 0;
				}
			}
		}
	}
	return bottom;
}
/*******************************************************************************
* Function Name  : ShowMode
* Description    : Print on LCD curren game mode

* Input          : mode - current game mode
* Output         : None
* Return         : None
*******************************************************************************/
void ShowMode(uint8_t mode)
{
#ifdef USE_WH1602		
	wh_set_cursor(0,0);
#endif

	switch (mode)
	{
		case CHGK:
		{
			wh_write_str_center("*��� ��� �����*",LINE0);
		}
		break;
		case BR:
		{
			wh_write_str_center("*����� ����*",LINE0);
		}
		break;
		case SI:
		{
			wh_write_str_center("*���� ����*",LINE0);
		}
		break;
		case VS:
		{
			wh_write_str_center("*� �������*",LINE0);
		}
		break;
	}
}




/*******************************************************************************
* Task Name  		 : vChangeModeTask
* Description    : Handler of MODE and VOLUME buttons
									if MODE pressed - switch to next MODE
									if VOLUME pressed - make volume louder (or silent if curren value is max)

* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void vChangeModeTask(void *pvParameters)
{
	uint16_t counter = 0;//,prev_mode,timerconfirm;
	volatile uint8_t state_select=0;

	while(1)
	{
		vTaskDelay(10);

		if (changeMode == RESETED)
		{
			//change game mode	
			if (PIN_SIGNAL(KEY1P)==0) //keyMODE was pressed
			{
				vTaskDelay(100);					//debounce protect, also we can increase this time for protecting from accident pressing
				if (PIN_SIGNAL(KEY1P)==0) //debounce protect
					{	
						BEEP(500,10,volume);
						state_select=1;
					
						if (counter >= 3) counter = 0; else	counter++; //switch to new mode
						ShowMode(counter); //show new mode on LCD
						playMode = counter;	
						
						while (PIN_SIGNAL(KEY1P)==0)
							state_select = 1;
										
					}
			}	
			//change volume
			if (PIN_SIGNAL(KEY2P)==0) //keyVOLUME was pressed
			{
				vTaskDelay(100);					//debounce protect, also we can increase this time for protecting from accident pressing
				if (PIN_SIGNAL(KEY2P)==0) //debounce protect
					{	

						#ifdef LEVELNEW   //InGame v1.0  
						
							while (PIN_SIGNAL(KEY2P)==0)
							{
								if (volume >= 16) 
								{
									wh_write_str_center("   ��� �����    ",LINE1);
									volume = 0; 
								}
								else	
								{
									volume+=4; //cicle of volume from 0 to 15
									lcdProgressBar(volume,LINE1);
									SetDAC1Level(((uint16_t)volume)*256/2);
								}
								vTaskDelay(200);	
							}
							BEEP(500,20,volume);
							vTaskDelay(500);
						
						#else	 	////InGame v1.1 
							
							if (volume >= 2) volume = 0; else	volume++; //cicle of volume from 0 to 2
							memset(str,0, sizeof(str));
							sprintf(str, "   VOLUME: %d   ", volume);
							#ifdef USE_WH1602	
								wh_write_str_center(str,LINE1);
							#endif
							BEEP(500,50,volume);
							while (PIN_SIGNAL(KEY2P)==0)	volume = volume;
							vTaskDelay(1500);
							
						#endif						
						wh_write_str_center("                ",LINE1);
					}
			}	
		}				
	}			
}

/*******************************************************************************
* Task Name  		 : vGameTask
* Description    : Main task - FSM of all game modes

* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void vGameTask(void *pvParameters)
{
	static volatile uint8_t prevchangeMode = 0;
	
	PIN_ON(BLIGHT);
	
	wh_lcd_clear();
	wh_write_str_center("INTELLECTUAL",LINE0);
	wh_write_str_center("GAME",LINE1);
	volume = 8;

	BEEP(500,40,volume);

	PIN_ON(BLIGHT);
	vTaskDelay(300);
	PIN_OFF(BLIGHT);
	vTaskDelay(300);
	PIN_ON(BLIGHT);
	vTaskDelay(300);
	PIN_OFF(BLIGHT);
	vTaskDelay(300);
	PIN_ON(BLIGHT);
	vTaskDelay(300);
	wh_lcd_clear();

	playMode = BR;
	changeMode=0;
	prevchangeMode=99;	
	ShowMode(playMode);
	
	// Main infinite loop
	while(1)
	{
		switch (playMode)
		{
			case CHGK:
			{
				changeMode=FSMCHGK();
			}
			break;
			case BR:
			{
				changeMode=FSMBR();
			}
			break;
			case SI:
			{
				changeMode=FSMSI();
			}
			break;
			case VS:
			{
				changeMode=FSMVS();
			}
			break;
		}
	}
}
/*******************************************************************************
* Function Name  : main
* Description    : Init all
									 Default game - BR
									 Default volume - 4
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
int main()
{
	char i;
	
	Set_HWSystem();
	playMode=BR;
	
	volume=4; // low volume
	
	for (i=0;i<8;i++) 	
		player[i]=InitPlayer;
	
	printf("Game Pult Controller v1\n\r");
	wh_lcd_clear();

	xTaskCreate(vGameTask,NULL, configMINIMAL_STACK_SIZE,	NULL, tskIDLE_PRIORITY + 1, NULL);
	xTaskCreate(vChangeModeTask,NULL, configMINIMAL_STACK_SIZE,	NULL, tskIDLE_PRIORITY + 2, NULL);

	vTaskStartScheduler();

}
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
// All Game FSM has to back value of current state: active or inactive
// if state is active - we can't change current mode
// Important!!!
// All Game FSM can't containes while(1) loops - all FSM must be out after one pass
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/

/*******************************************************************************
* Function Name  : FSMCHGK
* Description    : FSM for CHGK

	wait press start - generate BEEP_Start
	wait 50 sec - generate BEEP_Remind10sec
	wait 60 sec - generate BEEP_StopDiscuss
	
	if admin will press RESET again during 1 min interval - game will be stopped
	
	if admin will press PAUSE during 1 min interval - game will be paused
	if admin will press START during PAUSED state - game will start again from the paused time

* Input          : None
* Output         : None
* Return         : currentState of FSM
*******************************************************************************/
char FSMCHGK(void)
{
	switch (state)
		{
			case RESETED:
			{
				keyStartHandler(COUNTDOWN,0);
			}
			break;
			
			case COUNTDOWN:
			{
				printTime();
				
				timeEndingHandler(50);
				
				timeOverHandler(60);
		
				keyPauseHandler();

				keyResetHandler();		
			
			}
			break;
			
			case FINISH:
			{
				keyResetHandler();
				keyStartHandler(COUNTDOWN,0);	
			}
			break;
			
			case PAUSED:
			{
					keyStartHandler(COUNTDOWN,prev_time);		
					keyResetHandler();
			}
		}
		return state;
}
/*******************************************************************************
* Function Name  : FSMBR
* Description    : FSM for BR

	wait press START - generate BEEP_Start
	from 55 to 59 sec - generate BEEP_TimeIsEnding
	wait 60 sec - generate BEEP_StopDiscuss
	
	if Player press button before START - player get falstart
	if Player press button during discuss time - player answers, others aren't allowed to answer
	if Player's answer is correct - admin press RESTART for new question 
	else admin press START to start reduced time for discussion
	
	if admin will press RESET again during 1 min interval - game will be stopped
	
	if admin will press PAUSE during 1 min interval - game will be paused
	if admin will press START during PAUSED state - game will start again from the paused time

* Input          : None
* Output         : None
* Return         : currentState of FSM
*******************************************************************************/
char FSMBR(void)
{
	int i;
	
	switch (state)
		{
			case RESETED:
			{
				if (who_pressed >=0)
				{
					state = FALSTART;	
				}
				else
				{
					keyStartHandler(COUNTDOWN,0);
					keyResetHandler();
				}
			}
			break;
			
			case COUNTDOWN:
			{
				printTime();

				for (i = 55; i < 60; i++)
					timeEndingHandler(i);

				timeOverHandler(60);
				keyPauseHandler();
				keyResetHandler();

				if (who_pressed >=0)
				{
					state = ANSWER;					
				}					
			}
			break;
			
			case FINISH:
			{
				keyResetHandler();					
			}
			break;
			
			case PAUSED:
			{
					keyStartHandler(COUNTDOWN,prev_time);		
			}
			break;
			
			case FALSTART:
			{
				player[who_pressed].enabled = 0;
				player[who_pressed].keypressed = 1;
				player[who_pressed].ledstate = BLINKING3S;
				BEEP(300,100,volume);
				
				printPlayerTimestamp(who_pressed+1);
				
				vTaskDelay(1000);
				
				who_pressed = -1;
				
				//state = RESETED;
				
				while (!keyResetHandler())
				{
					who_pressed = -1;
				}
				
			}
			break;
			
			case ANSWER:
			{
				player[who_pressed].enabled = 0;
				player[who_pressed].keypressed = 1;
				player[who_pressed].ledstate = PERMANENT;
				BEEP(750,100,volume);
				
				printPlayerTimestamp(who_pressed+1);
				
				vTaskDelay(1000);  				
				
				who_pressed = -1;
				
				state = WAITTORECOUNTDOWN;
			}
			break;
			
			case WAITTORECOUNTDOWN:
			{
				keyStartHandler(RECOUNTDOWN,0);
				keyResetHandler();				
			}
			break;
			
			case RECOUNTDOWN:
			{
				printTime();
				for (i = 15; i < 20; i++)
					timeEndingHandler(i);				

				timeOverHandler(20);
				keyPauseHandler();
				keyResetHandler();

				if (who_pressed >=0)
				{
					state = ANSWER;					
				}					
			}
			break;
		}

	return state;
}
/*******************************************************************************
* Function Name  : FSMSI
* Description    : FSM for SI

	wait press START - generate BEEP_Start
	wait 7 sec - generate BEEP_StopDiscuss
	
	if Player press button before START - player get falstart
	if Player press button during discuss time - player answers, others aren't allowed to answer
	if Player's answer is correct - admin press RESTART for new question 
	else admin press RESTART and START to start countdown for discussion for other players
	
	if admin will press RESET again during 7 sec interval - game will be stopped

	PAUSE state is not allowed here

* Input          : None
* Output         : None
* Return         : currentState of FSM
*******************************************************************************/
char FSMSI(void)
{

	switch (state)
		{
			case RESETED:
			{
				if (who_pressed >=0)
				{
					state = FALSTART;	
				}
				else
				{
					keyStartHandler(COUNTDOWN,0);
					keyResetHandler();
				}
			}
			break;
			
			case COUNTDOWN:
			{
				printTime();
				timeOverHandler(7);
				keyResetHandler();

				if (who_pressed >=0)
				{
					state = ANSWER;					
				}					
			}
			break;
			
			case FINISH:
			{
				keyResetHandler();					
			}
			break;
			
			case FALSTART:
			{
				player[who_pressed].ledstate = BLINKING3S;
				player[who_pressed].keypressed = 1;
				BEEP(300,100,volume);
				
				printPlayerTimestamp(who_pressed+1);
				
				vTaskDelay(1000);
				
				who_pressed = -1;
				
				//state = RESETED;
				while (!keyResetHandler())
				{
					who_pressed = -1;
				}
			}
			break;
			
			case ANSWER:
			{
				if (player[who_pressed].keypressed == 1 && player[who_pressed].keytimestamp < 200)
				{
					//player had falstarted and pressed again before 2sec has passed from start
					state = FALSTART;
				}
				else
				{
					//player, even if he had falstarted, is able to answer
					player[who_pressed].keypressed = 1;
					player[who_pressed].enabled = 0;
					player[who_pressed].ledstate = PERMANENT;
					BEEP(750,100,volume);
					
					printPlayerTimestamp(who_pressed+1);
					
					who_pressed = -1;
					
					state = WAITTORECOUNTDOWN;
						
				}

			}
			break;
		
			case WAITTORECOUNTDOWN:
			{
				keyResetHandler();
			}
		}	
	
	return state;
}
/*******************************************************************************
* Function Name  : FSMVS
* Description    : FSM for VS

	players can press their buttons at any time
	the queue of answers will be displayed on the secon line of LCD
	if the player in the top of queue gives correct answer, admin should press RESET until the queue isn't empty
	else admin press RESET to remove this player from the queue and give an opportunity to the next player
	
	the button of the first player in queue will be enlightened permanently, 
	buttons of others players will be blinking until the admin will not press the RESET button to move queue	
	

* Input          : None
* Output         : None
* Return         : currentState of FSM
*******************************************************************************/
char FSMVS(void)
{

	char str[16];
	static uint8_t shift = 0;
	int i;
	static int top = -1, bottom = -1, del = -1;
	
	switch (state)
	{
		case RESETED:
		{
			if (who_pressed >=0)
			{
				state = ANSWER;
			}
			
			del = keyResetVSHandler(top,bottom);
			
			if (del == -2)
			{
				top = -1;
				bottom = -1;
			}
			else
			if (bottom != del)
			{
				bottom = del;
				state = PRINTANSWER;
			}
			
			//bottom = keyResetVSHandler(top,bottom);
		}
		break;
		
		case ANSWER:
		{
			queue[++top] = who_pressed+1;
			player[who_pressed].keypressed = 1;
			player[who_pressed].enabled = 0;
			BEEP(750,50,volume);
			
			who_pressed = -1;
			
			state = PRINTANSWER;
		}
		break;
		
		case PRINTANSWER:
		{
				player[queue[bottom+1]-1].ledstate = PERMANENT;
			
				for (i=bottom+2;i<=top;i++)
					player[queue[i]-1].ledstate = BLINKING;
			
				//fill the printline by the queue of players;
				memset(str,0, sizeof(str));
				shift = 0;
				for (i = bottom+1; i <=top; i++)
				{
					sprintf(str + shift ,"P%d",queue[i]);
					shift+=2;
				}
				
				#ifdef USE_WH1602	
					wh_write_str_center("                ",LINE1);				
					wh_write_str_center(str,LINE1);
				#endif				
				
				state = RESETED;
		}
		break;
		
	}	
	
	return state;
}
/*-----------------------------------------------------------*/
//warp over LCD print

/*******************************************************************************
* Function Name  : wh_write_str_center
* Description    : write string at center in selected line

* Input          : string - text to write
									 line - line to write (0 or 1)	
* Output         : None
* Return         : None
*******************************************************************************/
#define MAX_LENGHT_WH1602		16

void wh_write_str_center(char* string,uint8_t line)
{
	
	uint8_t size;//,x;
	char* ch=string;

	wh_write_str("                ");
	size=0;
	while (*ch++) size++;
	size--;
	if (size>MAX_LENGHT_WH1602) size=MAX_LENGHT_WH1602;
	wh_set_cursor(line,MAX_LENGHT_WH1602/2-size/2-1);
	wh_write_str(string);

}
/*-----------------------------------------------------------*/

