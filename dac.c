/*-----------------------------------------------------------*/
#include "dac.h"

//#define delay_DAC delay
/*-----------------------------------------------------------*/
#define NUMBER_OF_SAMPLES	32
/*-----------------------------------------------------------*/
uint32_t Sinus[NUMBER_OF_SAMPLES];
#define pi  3.14159

void Sinus_calculate(uint16_t level)
{
	int i;
  for(i=0; i<NUMBER_OF_SAMPLES; i++)
  {
		Sinus[i] = (uint16_t) ((sin(i*(pi/NUMBER_OF_SAMPLES))*level)+4095/2);
  }
}
/*-----------------------------------------------------------*/
const uint32_t sinus_12bitH[NUMBER_OF_SAMPLES] ={
2046,
2179,
2306,
2424,
2527,
2611,
2674,
2713,
2726,
2713,
2674,
2611,
2527,
2424,
2306,
2179,
2046,
1913,
1786,
1668,
1565,
1481,
1418,
1379,
1366,
1379,
1418,
1481,
1565,
1668,
1786,
1913
};
const uint32_t sinus_12bitL[NUMBER_OF_SAMPLES] ={
2046,
2071,
2095,
2117,
2136,
2152,
2163,
2171,
2173,
2171,
2163,
2152,
2136,
2117,
2095,
2071,
2046,
2021,
1997,
1975,
1956,
1940,
1929,
1921,
1919,
1921,
1929,
1940,
1956,
1975,
1997,
2021
};
const uint32_t sinus_12bitZ[NUMBER_OF_SAMPLES] ={0};
/*-----------------------------------------------------------*/
void DACinit(void)
{
	DAC1initconst();
	DAC2DMAinitconst();
}

void DAC_init(char level)
{

	DMA_InitTypeDef            DMA_InitStructure;
	TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
	GPIO_InitTypeDef           GPIO_InitStructure;
	DAC_InitTypeDef            DAC_InitStructure;

  
		SystemCoreClockUpdate();
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA  , ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC | RCC_APB1Periph_TIM7 | RCC_APB1Periph_TIM6, ENABLE);
		//Sinus_calculate();
		
		//INIT GPIO DAC Output 1
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    //TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; = 0;
    TIM_TimeBaseStructure.TIM_Period = SystemCoreClock/(NUMBER_OF_SAMPLES*1000);
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
 
    TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update);

    DMA_DeInit(DMA1_Channel2);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & DAC->DHR12RD;
		switch (level)
		{
			case 0:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitZ;
			}
			break;
			case 1:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitL;
			}
			break;
			case 2:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitH;
			}
			break;			
			default:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitL;
			}
		}
    
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = ARRAY_LENGHT(sinus_12bitL);
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel2, &DMA_InitStructure);
 
    DMA_Cmd(DMA1_Channel2, ENABLE);
		
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = 0;
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_T7_TRGO;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;//DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_1, &DAC_InitStructure);
 
    DAC_Cmd(DAC_Channel_1, ENABLE);
    DAC_DMACmd(DAC_Channel_1, ENABLE);
    TIM_Cmd(TIM7, ENABLE);		
	
}
/*-----------------------------------------------------------*/
void DAC1initconst(void)
{
	GPIO_InitTypeDef           GPIO_InitStructure;
	DAC_InitTypeDef            DAC_InitStructure;

  
	SystemCoreClockUpdate();
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA  , ENABLE);	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	//INIT GPIO DAC  Output 1
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

  DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = 0;
  DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;//DAC_OutputBuffer_Disable;
  DAC_Init(DAC_Channel_1, &DAC_InitStructure);
  DAC_Cmd(DAC_Channel_1, ENABLE);
	//DAC_SetChannel1Data(DAC_Align_12b_R,0);
	
}
/*-----------------------------------------------------------*/
void DAC2DMAinitconst(void)
{

	DMA_InitTypeDef            DMA_InitStructure;
	TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
	GPIO_InitTypeDef           GPIO_InitStructure;
	DAC_InitTypeDef            DAC_InitStructure;

  
		SystemCoreClockUpdate();
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA  , ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		//RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
		
		//INIT GPIO DAC  Output 2
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    //TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; = 0;
    TIM_TimeBaseStructure.TIM_Period = SystemCoreClock/(NUMBER_OF_SAMPLES*1000);
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
    TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update); //for DAC2
		
    DMA_DeInit(DMA1_Channel2);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & DAC->DHR12R2;
		DMA_InitStructure.DMA_MemoryBaseAddr=(uint32_t) &sinus_12bitH;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = ARRAY_LENGHT(sinus_12bitL);
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel3, &DMA_InitStructure);
    DMA_Cmd(DMA1_Channel3, ENABLE);
		
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = 0;
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;//DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_2, &DAC_InitStructure);
    DAC_Cmd(DAC_Channel_2, ENABLE);
    DAC_DMACmd(DAC_Channel_2, ENABLE);
		
    TIM_Cmd(TIM6, ENABLE); //for DAC2			
}
/*-----------------------------------------------------------*/
void SetDAC1Level(uint16_t level)
{
	DAC_SetChannel1Data(DAC_Align_12b_R,level);
}
/*-----------------------------------------------------------*/
void SetSound(uint32_t frequency)
{
	TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
	TIM_Cmd(TIM6, DISABLE);
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure); 
	TIM_TimeBaseStructure.TIM_Period = SystemCoreClock/(NUMBER_OF_SAMPLES*frequency);   ///   period=HCLK/(NUMBER_OF_SAMPLES*Fout) ; HCLK=16000000; for 500Hz P=178, for 1000Hz P=89   
	TIM_TimeBaseStructure.TIM_Prescaler = 0;       
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;    
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
 //TIM2 TRGO selection 
	TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);
	TIM_DMACmd(TIM6, TIM_DMA_Update, ENABLE);
	TIM_Cmd(TIM6, ENABLE);	
}
/*-----------------------------------------------------------*/
//Accept ENABLE or DISABLE
void GenerateSound(FunctionalState State)
{
	GPIO_InitTypeDef           GPIO_InitStructure;
	
    if (State==DISABLE)
		{
			GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_Init(GPIOA, &GPIO_InitStructure);
			GPIO_ResetBits(GPIOA,GPIO_Pin_5);
		}
		else
		{
			GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5;
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
			GPIO_Init(GPIOA, &GPIO_InitStructure);
			//delay(30);			
		}
		DAC_DMACmd(DAC_Channel_2, State);
    TIM_Cmd(TIM6, State);
		DAC_Cmd(DAC_Channel_2, State);
}
/*-----------------------------------------------------------*/
void BEEP(uint32_t frequency, uint32_t timeout, char level)
{
	#ifdef LEVELNEW
	//DAC1initconst();
	SetDAC1Level(0);
	//DAC2DMAinitconst();
//#ifdef LEVELNEW
//	DAC_init_var(level);
//#else	
//	DAC_init(level);
//#endif	
	GenerateSound(DISABLE);
	SetSound(frequency);
	GenerateSound(ENABLE);
	switch (level)
	{
		case 0: 	// silent
			break;
		case 4: 	// quiet
			level = 7;
			break;
		case 8: 	// medium
			break;
		case 12: 	// loud
			level=9;
			break;
		case 16:	// very loud
			level=15;
			break;
		default:
			level=8;
			break;
	}

	SetDAC1Level(((uint16_t)level)*256);
//	delay_DAC(timeout);
//	GenerateSound(DISABLE);
	SetBeepDelay(timeout);
	#else
		DAC_init(level);
		GenerateSound(DISABLE);
	SetSound(frequency);
	GenerateSound(ENABLE);
//	delay_DAC(timeout);
//	GenerateSound(DISABLE);
	SetBeepDelay(timeout);
	#endif
}
/*-----------------------------------------------------------*/
/*

//set level from 0 to 15 -> ampitude will be from 0 to 1920
void DAC_init_var(char level)
{

	DMA_InitTypeDef            DMA_InitStructure;
	TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
	GPIO_InitTypeDef           GPIO_InitStructure;
	DAC_InitTypeDef            DAC_InitStructure;

  
		SystemCoreClockUpdate();
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA  , ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		//RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
		
		//INIT GPIO DAC Output 1 Output 2
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4 | GPIO_Pin_5;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    //TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; = 0;
    TIM_TimeBaseStructure.TIM_Period = SystemCoreClock/(NUMBER_OF_SAMPLES*1000);
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
		//TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
    TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update); //for DAC2
		//TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update); //for DAC1
		
    DMA_DeInit(DMA1_Channel2);
		DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & DAC->DHR12R2;
	//calc sinus buffer	
		Sinus_calculate((level)*128); //input 0-15 out 0-1920
		DMA_InitStructure.DMA_MemoryBaseAddr=(uint32_t) &Sinus;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = ARRAY_LENGHT(sinus_12bitL);
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel3, &DMA_InitStructure);
    DMA_Cmd(DMA1_Channel3, ENABLE);
		
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = 0;
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;//DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_2, &DAC_InitStructure);
		DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;//DAC_Trigger_T7_TRGO;
		DAC_Init(DAC_Channel_1, &DAC_InitStructure);
		
 
    DAC_Cmd(DAC_Channel_1, ENABLE);
//    DAC_DMACmd(DAC_Channel_1, ENABLE);
		
    DAC_Cmd(DAC_Channel_2, ENABLE);
    DAC_DMACmd(DAC_Channel_2, ENABLE);
		
    TIM_Cmd(TIM6, ENABLE); //for DAC2			
	
}
//set discret level 0,1,2
void DAC_init(char level)
{

	DMA_InitTypeDef            DMA_InitStructure;
	TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
	GPIO_InitTypeDef           GPIO_InitStructure;
	DAC_InitTypeDef            DAC_InitStructure;

  
		SystemCoreClockUpdate();
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA  , ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		//RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
		
		//INIT GPIO DAC Output 1 Output 2
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4 | GPIO_Pin_5;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    //TIM_TimeBaseStructure.TIM_RepetitionCounter = 0; = 0;
    TIM_TimeBaseStructure.TIM_Period = SystemCoreClock/(NUMBER_OF_SAMPLES*1000);
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM6, &TIM_TimeBaseStructure);
		//TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
    TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update); //for DAC2
		//TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update); //for DAC1
		
    DMA_DeInit(DMA1_Channel2);
		DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & DAC->DHR12R2;
		switch (level)
		{
			case 0:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitZ;
			}
			break;
			case 1:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitL;
			}
			break;
			case 2:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitH;
			}
			break;			
			default:
			{
				DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &sinus_12bitL;
			}
		}
    
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = ARRAY_LENGHT(sinus_12bitL);
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel3, &DMA_InitStructure); //for DAC2 channel3. for DAC1 channel2
    DMA_Cmd(DMA1_Channel3, ENABLE);
		
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = 0;
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;//DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_2, &DAC_InitStructure);
		DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;//DAC_Trigger_T7_TRGO;
		DAC_Init(DAC_Channel_1, &DAC_InitStructure);
		
 
    DAC_Cmd(DAC_Channel_1, ENABLE);
//    DAC_DMACmd(DAC_Channel_1, ENABLE);
		
    DAC_Cmd(DAC_Channel_2, ENABLE);
    DAC_DMACmd(DAC_Channel_2, ENABLE);
		
    TIM_Cmd(TIM6, ENABLE); //for DAC2		
	
}



void SetSoundDAC1(uint32_t frequency)
{
	TIM_TimeBaseInitTypeDef    TIM_TimeBaseStructure;
	TIM_Cmd(TIM7, DISABLE);
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure); 
	TIM_TimeBaseStructure.TIM_Period = SystemCoreClock/(NUMBER_OF_SAMPLES*frequency);   ///   period=HCLK/(NUMBER_OF_SAMPLES*Fout) ; HCLK=16000000; for 500Hz P=178, for 1000Hz P=89   
	TIM_TimeBaseStructure.TIM_Prescaler = 0;       
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;    
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);
 //TIM2 TRGO selection 
	TIM_SelectOutputTrigger(TIM7, TIM_TRGOSource_Update);
	TIM_DMACmd(TIM7, TIM_DMA_Update, ENABLE);
	TIM_Cmd(TIM7, ENABLE);	
}
*/
/*-----------------------------------------------------------*/
